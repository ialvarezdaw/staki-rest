const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
  const token = req.get('token');

  jwt.verify(token, process.env.SEED, (err, decoded) => {
    if (err) {
      return res.status(401).json({
        ok: false,
        err: {
          message: 'Token no válido'
        }
      });
    }

    req.user = decoded.user;
    next();

  });
};

const verifyAdminRole = (req, res, next) => {
  const user = req.user;

  if (user.role === 'ADMIN_ROLE') {
    next();
  } else {
    return res.json({
      ok: false,
      err: {
        message: 'El usuario no es administrador'
      }
    });
  }
};

const verifyTokenImg = (req, res, next) => {
  const token = req.query.token;

  jwt.verify(token, process.env.SEED, (err, decoded) => {

    if (err) {
      return res.status(401).json({
        ok: false,
        err: {
          message: 'Token no válido'
        }
      });
    }

    req.user = decoded.user;
    next();

  });
};

module.exports = {
  verifyToken,
  verifyAdminRole,
  verifyTokenImg
};
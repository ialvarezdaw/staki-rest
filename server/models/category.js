const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const categorySchema = new Schema({
  name: {
    type: String,
    required: [true, 'Nombre es requerido'],
    unique: true
  },
  description: {
    type: String,
    required: false
  },
  img: {
    type: String,
    required: false
  },
  localGuide: {
    type: Boolean,
    required: false
  }
});

module.exports = mongoose.model('Category', categorySchema);
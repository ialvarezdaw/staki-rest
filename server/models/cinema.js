const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cinemaSchema = new Schema({
  title: { type: String, required: true },
  img: { type: String },
  synopsis: { type: String }
});

module.exports = mongoose.model('Cinema', cinemaSchema)
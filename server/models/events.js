const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eventsSchema = new Schema({
  name: { type: String, required: [true, 'Nombre es requerido']},
  description: { type: String, required: false },
  img: { type: String, required: false },
  dateStart: { type: Date, required: false },
  dateEnd: { type: Date, required: false },
  type: { type: String, required: false }
});

module.exports = mongoose.model('Events', eventsSchema);

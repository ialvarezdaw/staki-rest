const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const itemsSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Nombre es requerido']
  },
  price: {
    type: Number,
    required: false
  },
  description: {
    type: String,
    required: false
  },
  store: {
    type: Schema.Types.ObjectId,
    ref: 'Store',
    required: true
  }
});

module.exports = mongoose.model('Items', itemsSchema);
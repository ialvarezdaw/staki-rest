const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const jobsSchema = new Schema({
  company: { type: String, required: [true, 'Compañia es requerida']},
  description: { type: String, required: false },
  img: { type: String, required: false }
});

module.exports = mongoose.model('Jobs', jobsSchema);

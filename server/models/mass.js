const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const massSchema = new Schema({
  img: { type: String, required: false },
  name: { type: String, required: false },
});

module.exports = mongoose.model('Mass', massSchema);

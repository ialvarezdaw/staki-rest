const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const storeSchema = new Schema({
  name: { type: String, required: [true, 'Nombre es requerido'] },
  description: { type: String, required: false },
  email: { type: String, required: false, sparse: true },
  phone: { type: Number, required: false },
  schedule: { type: String },
  img: { type: String, required: false },
  gallery: [String],
  address: {},
  website: { type: String, required: false },
  twitter: { type: String, required: false },
  facebook: { type: String, required: false },
  instagram: { type: String, required: false },
  subcategory: [{
    type: Schema.Types.ObjectId,
    ref: 'Subcategory',
    required: false
  }]
});

module.exports = mongoose.model('Store', storeSchema);

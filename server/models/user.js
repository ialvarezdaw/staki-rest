const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const enumRoles = {
  values: ['ADMIN_ROLE', 'USER_ROLE', 'STORE_ROLE', 'DELIVERY_ROLE'],
  message: '{VALUE} rol no válido'
};

const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Nombre es requerido']
  },
  email: {
    type: String,
    unique: true,
    required: [true, 'El email es requerido']
  },
  password: {
    type: String,
    required: [true, 'El password es requerido']
  },
  phone: {
    type: Number,
    unique: true,
    required: true
  },
  address: {
    type: String,
    unique: true,
    required: true
  },
  dni: {
    type: String,
    unique: true,
    required: true
  },
  img: {
    type: String,
    required: false
  },
  role: {
    type: String,
    default: 'USER_ROLE',
    enum: enumRoles
  },
  status: {
    type: Boolean,
    default: true
  }
});


userSchema.methods.toJSON = function() {
  let user = this;
  let userObject = user.toObject();
  delete userObject.password;

  return userObject;
}

userSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' });
module.exports = mongoose.model('User', userSchema);

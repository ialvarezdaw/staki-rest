const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const workScheduleSchema = new Schema({
  name: { type: String, required: false },
  img: { type: String, required: false }
});

module.exports = mongoose.model('WorkSchedule', workScheduleSchema);

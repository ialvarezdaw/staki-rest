const express = require('express');

const Category = require('../models/category');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/category', (req, res) => {

  Category.find({}, 'name description img')
    .exec((err, categories) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          err
        });
      }

      Category.count({}, (err, total) => {
        res.json({
          ok: true,
          categories,
          total
        });
      });
  })

});

app.get('/api/v1/category/local-guide', (req, res) => {
  Category.find({'localGuide': true}, 'name description img')
  .exec((err, categories) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    Category.count({}, (err, total) => {
      res.json({
        ok: true,
        categories,
        total
      });
    });
  })

});



app.get('/api/v1/category/:id', (req, res) => {
  const id = req.params.id;

  Category.findById(id, (err, categoryDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!categoryDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no es correcto'
        }
      });
    }

    res.json({
      ok: true,
      category: categoryDB
    });
  });
});

app.post('/api/v1/category', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const category = new Category({
    name: body.name,
    description: body.description,
    img: body.img,
    localGuide: body.localGuide
  });

  category.save((err, categoryDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!categoryDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      category: categoryDB
    })
  });
});

app.put('/api/v1/category/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  let updCategory = {
    name: body.name,
    description: body.description,
    localGuide: body.localGuide
  };

  if (body.img) {
    updCategory.img = body.img;
  }

  Category.findByIdAndUpdate(id, updCategory, { new: true, runValidators: true}, (err, categoryDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!categoryDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      category: categoryDB
    });
  });
});

app.delete('/api/v1/category/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  Category.findByIdAndRemove(id, (err, categoryDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!categoryDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Categoria borrada'
    });
  });
})

module.exports = app;

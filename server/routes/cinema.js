const express = require('express');

const Cinema = require('../models/cinema');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/cinema', (req, res) => {
  let offset = req.query.offset || 0;
  offset = Number(offset);

  let limit = req.query.limit || 5;
  limit = Number(limit);

  Cinema.find({}, 'title')
    .skip(offset)
    .limit(limit)
    .exec((err, cinema) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          err
        });
      }

      Cinema.count({}, (err, total) => {
        res.json({
          ok: true,
          cinema,
          total
        });
      });
  });
});

app.get('/api/v1/cinema/:id', (req, res) => {
  const id = req.params.id;

  Cinema.findById(id, (err, cinemaDB) => {

    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!cinemaDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no es correcto'
        }
      });
    }

    res.json({
      ok: true,
      cinema: cinemaDB
    });
  });
});

app.post('/api/v1/cinema', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const cinema = new Cinema({
    title: body.title,
    img: body.img,
    synopsis: body.synopsis
  });

  cinema.save((err, cinemaDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!cinemaDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      cinema: cinemaDB
    })
  });
});

app.put('/api/v1/cinema/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  let updateCine = {
    title: body.title,
    synopsis: body.synopsis
  };

  if (body.img) {
    updateCine.img= body.img;
  }

  Cinema.findByIdAndUpdate(id, updateCine, { new: true, runValidators: true}, (err, cinemaDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!cinemaDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      cinema: cinemaDB
    });
  });
});

app.delete('/api/v1/cinema/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  Cinema.findByIdAndRemove(id, (err, cinemaDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!cinemaDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Pelicula borrada'
    });
  });
})

module.exports = app;

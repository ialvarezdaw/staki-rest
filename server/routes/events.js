const express = require('express');

const Events = require('../models/events');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/events', (req, res) => {
  Events.find({}, 'name')
  .exec((err, events) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    Events.count({}, (err, total) => {
      res.json({
        ok: true,
        events,
        total
      });
    });
  });
});

app.get('/api/v1/events/:id', (req, res) => {
  const id = req.params.id;

  Events.findById(id, (err, eventDB) => {

    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!eventDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no es correcto'
        }
      });
    }

    res.json({
      ok: true,
      event: eventDB
    });
  });
});

app.post('/api/v1/events', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const events = new Events({
    name: body.name,
    img: body.img,
    dateStart: body.dateStart,
    dateEnd: body.dateEnd,
    type: body.type,
    description: body.description
  });

  events.save((err, eventDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!eventDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      event: eventDB
    })
  });
});

app.put('/api/v1/events/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  let updateEvent = {
    name: body.name,
    type: body.type,
    description: body.description
  };

  if (body.img) {
    updateEvent.img= body.img;
  }

  if (body.dateStart) {
    updateEvent.dateStart= body.dateStart;
  }

  if (body.dateEnd) {
    updateEvent.dateEnd = body.dateEnd;
  }

  Events.findByIdAndUpdate(id, updateEvent, { new: true, runValidators: true}, (err, eventDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!eventDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      event: eventDB
    });
  });
});

app.delete('/api/v1/events/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  Events.findByIdAndRemove(id, (err, eventDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!eventDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Evento borrado'
    });
  });
})

module.exports = app;

const express = require('express');

const app = express();

app.use(require('./category'));
app.use(require('./cinema'));
app.use(require('./imagenes'));
app.use(require('./item'));
app.use(require('./login'));
app.use(require('./store'));
app.use(require('./subcategory'));
app.use(require('./upload'));
app.use(require('./user'));
app.use(require('./events'));
app.use(require('./schedule-sports'));
app.use(require('./jobs'));
app.use(require('./mass'));
app.use(require('./work-schedule'));
app.use(require('./phones'));

module.exports = app;

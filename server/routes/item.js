const express = require('express');

const Item = require('../models/items');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/item', (req, res) => {
  Item.find({}, 'name price description')
    .populate('store', 'name email phone category')
    .exec((err, items) => {

      if (err) {
        return res.status(500).json({
          ok: false,
          err
        });
      }

      Item.count({}, (err, total) => {
        res.json({
          ok: true,
          items,
          total
        });
      });
  });

});

app.get('/api/v1/item/:id', (req, res) => {
  const id = req.params.id;

  Item.findById(id, (err, itemDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!itemDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no es correcto'
        }
      })
    }

    res.json({
      ok: true,
      item: itemDB
    });
  })
});

app.post('/api/v1/item', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const item = new Item({
    name: body.name,
    price: body.price,
    description: body.description,
    store: body.store
  });

  item.save((err, itemDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!itemDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.status(201).json({
      ok: true,
      item: itemDB
    });
  });
});

app.put('/api/v1/item/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  let updItem = {
    name: body.name,
    price: body.price,
    description: body.description,
    store: body.store
  };

  Item.findByIdAndUpdate(id, updItem, { new: true, runValidators: true}, (err, itemDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!itemDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      item: itemDB
    });
  });
});

app.delete('/api/v1/item/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;

  Item.FindByIdAndRemove(id, (err, itemDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!itemDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Item borrado'
    });
  });
})

module.exports = app;

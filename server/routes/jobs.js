const express = require('express');

const Jobs = require('../models/jobs');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/jobs', (req, res) => {
  Jobs.find({}, 'company')
  .exec((err, jobs) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    Jobs.count({}, (err, total) => {
      res.json({
        ok: true,
        jobs,
        total
      });
    });
  });
});

app.get('/api/v1/jobs/:id', (req, res) => {
  const id = req.params.id;

  Jobs.findById(id, (err, jobDB) => {

    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!jobDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no es correcto'
        }
      });
    }

    res.json({
      ok: true,
      job: jobDB
    });
  });
});

app.post('/api/v1/jobs', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const jobs = new Jobs({
    company: body.company,
    img: body.img,
    description: body.description
  });

  jobs.save((err, jobDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!jobDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      job: jobDB
    })
  });
});

app.put('/api/v1/jobs/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  let updateJob = {
    company: body.company,
    description: body.description
  };

  if (body.img) {
    updateJob.img= body.img;
  }

  Jobs.findByIdAndUpdate(id, updateJob, { new: true, runValidators: true}, (err, jobDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!jobDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      job: jobDB
    });
  });
});

app.delete('/api/v1/jobs/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  Jobs.findByIdAndRemove(id, (err, jobDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!jobDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Oferta trabajo borrada'
    });
  });
})

module.exports = app;

const express = require('express');

const Mass = require('../models/mass');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/mass', (req, res) => {
  Mass.find({}, 'name').exec((err, mass) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    Mass.count({}, (err, total) => {
      res.json({
        ok: true,
        mass,
        total
      });
    });
  });
});

app.get('/api/v1/mass/:id', (req, res) => {
  const id = req.params.id;

  Mass.findById(id, (err, massDB) => {

    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!massDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no es correcto'
        }
      });
    }

    res.json({
      ok: true,
      mass: massDB
    });
  });
});

app.post('/api/v1/mass', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const mass = new Mass({
    name: body.name,
    img: body.img,
  });

  mass.save((err, massDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!massDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      mass: massDB
    })
  });
});

app.put('/api/v1/mass/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  let updateMass = {
    name: body.name,
    img: body.img
  };

  Mass.findByIdAndUpdate(id, updateMass, { new: true, runValidators: true}, (err, massDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!massDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      mass: massDB
    });
  });
});

app.delete('/api/v1/mass/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  Mass.findByIdAndRemove(id, (err, massDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!massDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Misa borrada'
    });
  });
})

module.exports = app;

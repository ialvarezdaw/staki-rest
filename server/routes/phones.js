const express = require('express');
const Phones = require('../models/phones');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');
const app = express();

app.get('/api/v1/phones', (req, res) => {
  Phones.find({}, 'name phone').exec((err, phones) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    Phones.count({}, (err, total) => {
      res.json({
        ok: true,
        phones,
        total
      });
    });
  });
});

app.get('/api/v1/phones/:id', (req, res) => {
  const id = req.params.id;

  Phones.findById(id, (err, phoneDB) => {

    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!phoneDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no es correcto'
        }
      });
    }

    res.json({
      ok: true,
      phone: phoneDB
    });
  });
});

app.post('/api/v1/phones', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const phones = new Phones({
    name: body.name,
    phone: body.phone,
  });

  phones.save((err, phoneDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!phoneDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      phone: phoneDB
    });
  });
});

app.put('/api/v1/phones/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  let updatePhone = {
    name: body.name,
    phone: body.phone
  };

  Phones.findByIdAndUpdate(id, updatePhone, { new: true, runValidators: true}, (err, phoneDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!phoneDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      phone: phoneDB
    });
  });
});

app.delete('/api/v1/phones/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  Phones.findByIdAndRemove(id, (err, phoneDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!phoneDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Teléfono borrad'
    });
  });
})

module.exports = app;

const express = require('express');

const Sports = require('../models/schedule-sports');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/sports', (req, res) => {
  Sports.find({}, 'name').exec((err, sports) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    Sports.count({}, (err, total) => {
      res.json({
        ok: true,
        sports,
        total
      })
    });
  });
});

app.get('/api/v1/sports/:id', (req, res) => {
  const id = req.params.id;

  Sports.findById(id, (err, sportsDB) => {

    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!sportsDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no es correcto'
        }
      });
    }

    res.json({
      ok: true,
      sport: sportsDB
    });
  });
})


app.post('/api/v1/sports', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const sports = new Sports({
    name: body.name,
    img: body.img,
    dateStart: body.dateStart,
    dateEnd: body.dateEnd,
    type: body.type,
    description: body.description
  });

  sports.save((err, sportDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!sportDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      sport: sportDB
    })
  });
});


app.put('/api/v1/sports/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  let updateSport = {
    name: body.name,
    type: body.type,
    description: body.description
  };

  if (body.img) {
    updateSport.img= body.img;
  }

  if (body.dateStart) {
    updateSport.dateStart= body.dateStart;
  }

  if (body.dateEnd) {
    updateSport.dateEnd = body.dateEnd;
  }

  Sports.findByIdAndUpdate(id, updateSport, { new: true, runValidators: true}, (err, sportDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!sportDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      sport: sportDB
    });
  });
});

app.delete('/api/v1/sports/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  Sports.findByIdAndRemove(id, (err, sportDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!sportDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Evento Deportivo borrado'
    });
  });
})

module.exports = app;

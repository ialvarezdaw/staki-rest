const express = require('express');

const Store = require('../models/store');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/store', (req, res) => {

  Store.find({}, 'name')
    .populate('subcategory', 'name description')
    .exec((err, stores) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          err
        });
      }

      res.json({
        ok: true,
        stores
      });
  })
});

app.get('/api/v1/store/subcategory/:id', (req, res) => {
  const subId = req.params.id;
  Store.find({'subcategory': subId}, 'name img phone')
  .exec((err, stores) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      stores
    });
  })
});

app.get('/api/v1/store/:id', (req, res) => {
  const id = req.params.id;

  Store.findById(id)
    .populate('subcategory', 'name description')
    .exec((err, storeDB) => {

      if (err) {
        return res.status(500).json({
          ok: false,
          err
        });
      }

      if (!storeDB) {
        return res.status(400).json({
          ok: false,
          err: {
            message: 'El ID no existe'
          }
        });
      }

      res.json({
        ok: true,
        store: storeDB
      });
  })
});

app.post('/api/v1/store', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const store = new Store({
    name: body.name,
    email: body.email !== '' ? body.email : null,
    phone: body.phone,
    description: body.description,
    img: body.img,
    address: body.address,
    subcategory: body.subcategory,
    schedule: body.schedule,
    website: body.website,
    twitter: body.twitter,
    facebook: body.facebook,
    instagram: body.instagram,
    gallery: body.galeria,
  });

  store.save((err, storeDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    res.status(201).json({
      ok: true,
      store: storeDB
    });
  })
});

app.put('/api/v1/store/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  Store.findById(id, (err, storeDB) => {

    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!storeDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    storeDB.name = body.name;
    storeDB.email = body.email !== '' ? body.email : null;
    storeDB.phone = body.phone;
    storeDB.description = body.description;
    storeDB.schedule = body.schedule;
    storeDB.website = body.website;
    storeDB.twitter = body.twitter;
    storeDB.facebook = body.facebook;
    storeDB.instagram = body.instagram;

    if (body.subcategory) {
      storeDB.subcategory = body.subcategory;
    }

    if (body.address) {
      storeDB.address = body.address;
    }

    if (body.img) {
      storeDB.img = body.img;
    }

    if (body.galeria) {
      storeDB.gallery.push(body.galeria);
    }

    storeDB.save((err, storeSaved) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          err
        });
      }

      res.status(201).json({
        ok: true,
        store: storeSaved
      });
    });

  });
});

app.delete('/api/v1/store/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;

  Store.findByIdAndRemove(id, (err, storeDB) => {

    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!storeDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Tienda borrada'
    });
  });
});

app.put('/api/v1/store/:id/gallery/:galleryId', [verifyToken, verifyAdminRole], (req, res) => {
  const storeId = req.params.id;
  const galleryId = req.params.galleryId;
  Store.findById(storeId, (err, storeDB) => {
    storeDB.gallery.pull(storeDB.gallery[galleryId]);
    storeDB.save((err, storeSaved) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          err
        });
      }

      res.status(201).json({
        ok: true,
        store: storeSaved
      });
    });
  });
});

module.exports = app;

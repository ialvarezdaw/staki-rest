const express = require('express');

const Subcategory = require('../models/subcategory');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/subcategory', (req, res) => {
  Subcategory.find({}, 'name description')
    .populate('category', 'name description')
    .exec((err, subcategories) => {
     if(err) {
       return res.status(500).json({
         ok: false,
         err
       });
     }

     Subcategory.count({}, (err, total) => {
       res.json({
         ok: true,
         subcategories,
         total
       });
     });
  });
});

app.get('/api/v1/subcategory/category/:id', (req, res) => {
  const categoryId = req.params.id;
  Subcategory.find({'category': categoryId})
    .populate('category', 'name description category img')
    .exec((err, subcategories) => {
      if(err) {
        return res.status(500).json({
          ok: false,
          err
        });
      }

      Subcategory.count({}, (err, total) => {
        res.json({
          ok: true,
          subcategories,
          total
        });
    });
  })
})

app.get('/api/v1/subcategory/:id', (req, res) => {
  const id = req.params.id;
  Subcategory.findById(id)
    .populate('category', 'name description category img')
    .exec((err, subcategoryDB) => {

      if (err) {
        return res.status(500).json({
          ok: false,
          err
        });
      }

    if (!subcategoryDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      subcategory: subcategoryDB
    });
  });
});

app.post('/api/v1/subcategory', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const subcategory = new Subcategory({
    name: body.name,
    description: body.description,
    img: body.img,
    category: body.category
  });

  subcategory.save((err, subcategoryDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!subcategoryDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.status(201).json({
      ok: true,
      subcategory: subcategoryDB
    })
  });
});

/**
 * Actualizar subcategoría
 */
app.put('/api/v1/subcategory/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  Subcategory.findById(id, (err, subcategoryDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!subcategoryDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    subcategoryDB.name = body.name;
    subcategoryDB.description = body.description;
    subcategoryDB.category = body.category;
    if (body.img) {
      subcategoryDB.img = body.img;
    }

    subcategoryDB.save((err, subSaved) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          err
        });
      }

      res.status(201).json({
        ok: true,
        subcategory: subSaved
      });
    });
  });
});

app.delete('/api/v1/subcategory/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  Subcategory.findByIdAndRemove(id, (err, subcategoryDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!subcategoryDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Subcategoria borrada'
    });
  });
})

module.exports = app;

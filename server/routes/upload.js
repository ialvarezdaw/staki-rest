const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

const Category = require('../models/category');
const Subcategory = require('../models/subcategory');
const Store = require('../models/store');
const User = require('../models/user');
const Cinema = require('../models/cinema');
const Event = require('../models/events');

const fs = require('fs');
const path = require('path');

app.use(fileUpload());

app.put('/api/v1/upload/:tipo/:id', function(req, res) {
  let tipo = req.params.tipo;
  let id = req.params.id;

  if (!req.files) {
    return res.status(400).json({
      ok: false,
      err: {
        message: 'No ha seleccionado ningún archivo'
      }
    });
  }

  // Valida tipo
  let tiposValidos = ['store', 'category', 'subcategory', 'user', 'items', 'cinema', 'events'];
  if (tiposValidos.indexOf(tipo) < 0) {
    return res.status(400).json({
      ok: false,
      err: {
        message: 'Los tipos permitidas son ' + tiposValidos.join(', ')
      }
    })
  }

  let archivo;

  if (req.files.archivo) {
    archivo = req.files.archivo;
  }

  if (req.files.gallery) {
    archivo = req.files.gallery
  }

  let splitFile = archivo.name.split('.');
  let extension = splitFile[splitFile.length - 1];

  const extensionesValidas = ['png', 'jpg', 'jpeg', 'gif', 'svg', 'pdf'];
  if (extensionesValidas.indexOf(extension) < 0) {
    return res.status(400).json({
      ok: false,
      err: {
        message: "Las extensiones permitidas son " + extensionesValidas.join(', '),
      }
    })
  }

  let nombreArchivo = `${ id }-${ new Date().getMilliseconds()  }.${ extension }`;

  // Use the mv() method to place the file somewhere on your server
  archivo.mv(`uploads/${tipo}/${nombreArchivo}`, (err) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err,
        message: 'move image'
      });
    }

    if  (req.files.gallery) {
      tipo = 'galleryStore'
    }

    // Imagen cargada
    switch (tipo) {
      case 'category':
        categoryImage(id, res, nombreArchivo);
        break;
      case 'subcategory':
        subcategoryImage(id, res, nombreArchivo);
        break;
      case 'user':
        userImage(id, res, nombreArchivo);
        break;
      case 'store':
        storeImage(id, res, nombreArchivo);
        break;
      case 'galleryStore':
        storeGalleryImage(id, res, nombreArchivo);
        break;
      case 'cinema':
        cinemaImage(id, res, nombreArchivo);
        break;
      case 'events':
        eventImage(id, res, nombreArchivo);
        break;
    }

    res.status(200).json({
      ok: true,
      message: 'Imagen subida correctamente'
    });
  });
});

function categoryImage(id, res, nombreArchivo) {
  Category.findById(id, (err, categoryDB) => {

    if (err) {
      borraArchivo(nombreArchivo, 'category');

      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!categoryDB) {

      borraArchivo(nombreArchivo, 'category');

      return res.status(400).json({
        ok: false,
        err: {
          message: 'Categoria no existe'
        }
      });
    }

    borraArchivo(categoryDB.img, 'category')

    categoryDB.img = nombreArchivo;

    categoryDB.save((err, categorySaved) => {
    });
  });
}

function subcategoryImage(id, res, nombreArchivo) {
  Subcategory.findById(id, (err, subcategoryDB) => {
    if (err) {
      borraArchivo(nombreArchivo, 'subcategory');

      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!subcategoryDB) {
      borraArchivo(nombreArchivo, 'subcategory');

      return res.status(400).json({
        ok: false,
        err: {
          message: 'Subcategoria no existe'
        }
      });
    }

    borraArchivo(subcategoryDB.img, 'subcategory');

    subcategoryDB.img = nombreArchivo;

    subcategoryDB.save((err, subcategorySaved) => { });
  });
}

function userImage(id, res, nombreArchivo) {
  User.findById(id, (err, userDB) => {
    if (err) {
      borraArchivo(nombreArchivo, 'user');

      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!userDB) {
      borraArchivo(nombreArchivo, 'user');

      return res.status(400).json({
        ok: false,
        err: {
          message: 'Usuario no existe'
        }
      });
    }

    borraArchivo(userDB.img, 'user');

    userDB.img = nombreArchivo;

    userDB.save((err, userSaved) => {});
  });
}

function storeImage(id, res, nombreArchivo) {
  Store.findById(id, (err, storeDB) => {
    if (err) {
      borraArchivo(nombreArchivo, 'store');

      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!storeDB) {
      borraArchivo(nombreArchivo, 'store');

      return res.status(400).json({
        ok: false,
        err: {
          message: 'Tienda no existe'
        }
      });
    }

    borraArchivo(storeDB.img, 'store');

    storeDB.img = nombreArchivo;

    storeDB.save((err, storeSaved) => {});
  });
}

function storeGalleryImage(id, res, nombreArchivo) {
  Store.findById(id, (err, storeDB) => {
    if (err) {
      borraArchivo(nombreArchivo, 'store');
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!storeDB) {
      borraArchivo(nombreArchivo, 'store');

      return res.status(400).json({
        ok: false,
        err: {
          message: 'Tienda no existe'
        }
      });
    }

    storeDB.gallery.push(nombreArchivo);

    storeDB.save((err, storeSaved) => {});
  })
}

function cinemaImage(id, res, nombreArchivo) {
  Cinema.findById(id, (err, cinemaDB) => {
    if (err) {
      borraArchivo(nombreArchivo, 'cinema');

      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!cinemaDB) {
      borraArchivo(nombreArchivo, 'cinema');

      return res.status(400).json({
        ok: false,
        err: {
          message: 'Movie no existe'
        }
      });
    }

    borraArchivo(cinemaDB.img, 'cinema');

    cinemaDB.img = nombreArchivo;

    cinemaDB.save((err) => {});
  });
}

function eventImage (id, res, nombreArchivo) {
  Event.findById(id, (err, eventDB) => {
    if (err) {
      borraArchivo(nombreArchivo, 'events');

      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!eventDB) {
      borraArchivo(nombreArchivo, 'events');

      return res.status(400).json({
        ok: false,
        err: {
          message: 'Evento no existe'
        }
      });
    }

    borraArchivo(eventDB.img, 'events');

    eventDB.img = nombreArchivo;

    eventDB.save((err) => {});
  });
}

function borraArchivo(nombreImagen, tipo) {
  let pathImagen = path.resolve(__dirname, `../../uploads/${ tipo }/${ nombreImagen }`);
  if (fs.existsSync(pathImagen)) {
    fs.unlinkSync(pathImagen);
  }
}

module.exports = app;
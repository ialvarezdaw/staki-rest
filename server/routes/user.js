const express = require('express');

const bcrypt = require('bcrypt');
const _ = require('underscore');

const User = require('../models/user');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/user', verifyToken, (req, res) => {
  User.find({ status: true }, 'name email role img')
    .exec((err, users) => {
      if (err) {
        return res.status(400).json({
          ok: false,
          err
        });
      }

      User.count({ status: true }, (err, total) => {
        res.json({
          ok: true,
          users,
          total
        });

      });
    }
  );

});

app.post('/api/v1/user', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const user = new User({
    name: body.name,
    email: body.email,
    password: bcrypt.hashSync(body.password, 10),
    role: body.role || 'USER_ROLE',
    phone: body.phone || '',
    address: body.address || '',
    dni: body.dni || ''
  });

  user.save((err, userDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      user: userDB
    });
  });
});

app.put('/api/v1/user/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = _.pick(req.body, ['name', 'email', 'img', 'role', 'status']);

  User.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, userDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      user: userDB
    });

  });
});

app.delete('/api/v1/user/:id', [verifyToken, verifyAdminRole], function(req, res) {
  const id = req.params.id;
  const changeStatus = {
    status: false
  };

  User.findByIdAndUpdate(id, changeStatus, { new: true }, (err, userDeleted) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    if (!userDeleted) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'Usuario no encontrado'
        }
      });
    }

    res.json({
      ok: true,
      user: userDeleted
    });

  });
});

module.exports = app;
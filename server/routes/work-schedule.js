const express = require('express');

const WorkSchedule = require('../models/work-schedule');
const { verifyToken, verifyAdminRole } = require('../middlewares/authentication');

const app = express();

app.get('/api/v1/works-schedule', (req, res) => {
  WorkSchedule.find({}, 'name').exec((err, worksSchedule) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    WorkSchedule.count({}, (err, total) => {
      res.json({
        ok: true,
        worksSchedule,
        total
      });
    });
  });
});

app.get('/api/v1/works-schedule/:id', (req, res) => {
  const id = req.params.id;

  WorkSchedule.findById(id, (err, workDB) => {

    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!workDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no es correcto'
        }
      });
    }

    res.json({
      ok: true,
      workSchedule: workDB
    });
  });
});

app.post('/api/v1/works-schedule', [verifyToken, verifyAdminRole], (req, res) => {
  const body = req.body;

  const worksSchedule = new WorkSchedule({
    name: body.name,
    img: body.img,
  });

  worksSchedule.save((err, workDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!workDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      workSchedule: workDB
    })
  });
});

app.put('/api/v1/works-schedule/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  const body = req.body;

  let updateWorkSchedule = {
    name: body.name,
    img: body.img
  };

  WorkSchedule.findByIdAndUpdate(id, updateWorkSchedule, { new: true, runValidators: true}, (err, workDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!workDB) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      workSchedule: workDB
    });
  });
});

app.delete('/api/v1/works-schedule/:id', [verifyToken, verifyAdminRole], (req, res) => {
  const id = req.params.id;
  WorkSchedule.findByIdAndRemove(id, (err, workDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      });
    }

    if (!workDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'El ID no existe'
        }
      });
    }

    res.json({
      ok: true,
      message: 'Calendario borrado'
    });
  });
})

module.exports = app;

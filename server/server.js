require('./config/config');

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();

const bodyParser = require('body-parser');

let corsOptions;
// Enable CORS to local and prod development.
if (process.env.NODE_ENV  === 'dev') {
  const whitelist = ['http://localhost:4200', 'http://localhost:3000', 'http://localhost:8080'];
  corsOptions = {
    origin: function (origin, callback) {
      if (whitelist.indexOf(origin) !== -1) {
        callback(null, true)
      } else {
        callback(new Error('Not allowed by CORS'))
      }
    }
  }
} else {
  const whitelist = ['https://staki-admin.herokuapp.com'];
  corsOptions = {
    origin: function (origin, callback) {
      if (whitelist.indexOf(origin) !== -1) {
        callback(null, true)
      } else {
        callback(new Error('Not allowed by CORS'))
      }
    }
  }
}
app.use(cors(corsOptions));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json());

// Configuración global de rutas
app.use(require('./routes/index'));

mongoose.connect(process.env.URLDB, { useNewUrlParser: true }, (err) => {

  if (err) throw err;

});


app.listen(process.env.PORT, () => {
  console.log('Listen port: ', process.env.PORT);
});